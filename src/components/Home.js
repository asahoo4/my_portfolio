// Home.js

import React from 'react';
import './Home.css'; // You can create a separate CSS file for styling if needed

const Home = () => (
  <div className="home-container">
    <h2>Aishani Sahoo</h2>
    <p>847-315-0350 · asahoo4@illinois.edu</p>
    <p>Hello! I'm Aishani Sahoo, a passionate Computer Science and Linguistics student at the University of Illinois at Urbana-Champaign. Welcome to my portfolio website where I showcase my skills, education, projects, and experiences.</p>
    <div className="social-icons">
      <a href="https://www.linkedin.com/in/asahoo0/" target="_blank" rel="noopener noreferrer">
        <i className="fa fa-linkedin"></i>
      </a>
      <a href="https://github.com/asahoo0" target="_blank" rel="noopener noreferrer">
        <i className="fa fa-github"></i>
      </a>
      
    </div>
  </div>
);

export default Home;
