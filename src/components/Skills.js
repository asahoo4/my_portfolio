// Skills.js

import React from 'react';
import './Skills.css';

const Skills = () => (
  <div className="skills-container">
    <h2 className="skills-heading">Technical Skills</h2>
    <div className="skills-list">
      <p>
        <strong>Languages:</strong> Python, SQL, R, Javascript, HTML/CSS, C++ <br />
        <strong>Libraries and Frameworks:</strong> React, TensorFlow, Pandas, NumPy, Matplotlib, NLTK, Plotly, D3.js, Spark, Scikit <br />
        <strong>Other Tools:</strong> Tableau, Git, Excel, Canva, Visual Studio
      </p>
    </div>
  </div>
);

export default Skills;
